package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;
import com.mojang.authlib.GameProfile;

@Mixin(net.minecraft.entity.player.PlayerEntity.class)
public interface PlayerEntityAccessor {
  @Accessor("gameProfile") @Mutable void gameProfile (GameProfile profile);
}
