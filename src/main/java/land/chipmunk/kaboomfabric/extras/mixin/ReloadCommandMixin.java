package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.server.command.ServerCommandSource;
import java.util.Collection;

@Mixin(net.minecraft.server.command.ReloadCommand.class)
public abstract class ReloadCommandMixin {
  @Inject(at = @At("HEAD"), method = "tryReloadDataPacks", cancellable = true)
  private static void tryReloadDataPacks (Collection<?> datapacks, ServerCommandSource source, CallbackInfo info) {
    info.cancel();
  }
}
