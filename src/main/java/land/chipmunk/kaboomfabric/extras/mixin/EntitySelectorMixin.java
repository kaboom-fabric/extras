package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.server.command.ServerCommandSource;

@Mixin(net.minecraft.command.EntitySelector.class)
public abstract class EntitySelectorMixin {
  @Inject(at = @At("HEAD"), method = "checkSourcePermission", cancellable = true)
  private void checkSourcePermission (ServerCommandSource source, CallbackInfo info) {
    info.cancel();
  }
}
