package land.chipmunk.kaboomfabric.extras;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.minecraft.util.Formatting;
import com.mojang.brigadier.CommandDispatcher;
import land.chipmunk.kaboomfabric.extras.commands.*;

public class Extras implements ModInitializer {
  public static final Logger LOGGER = LoggerFactory.getLogger("extras");
  private static final char[] escapeCodes = "0123456789abcdefklmnorx".toCharArray();

  @Override
  public void onInitialize() {
  }

  public static void registerCommands (CommandDispatcher dispatcher) {
    // CommandBroadcastMM.register(dispatcher);
    CommandBroadcastVanilla.register(dispatcher);
    CommandClearChat.register(dispatcher);
    CommandConsole.register(dispatcher);
    CommandDestroyEntities.register(dispatcher);
    CommandEnchantAll.register(dispatcher);
    // CommandGetJSON.register(dispatcher);
    CommandJumpscare.register(dispatcher);
    CommandKaboom.register(dispatcher);
    CommandPing.register(dispatcher);
    // CommandPrefix.register(dispatcher);
    CommandPumpkin.register(dispatcher);
    CommandServerInfo.register(dispatcher);
    // CommandSkin.register(dispatcher);
    CommandSpawn.register(dispatcher);
    CommandSpidey.register(dispatcher);
    CommandTellraw.register(dispatcher);
    CommandUsername.register(dispatcher);
  }

  public static String parseEscapeSequences (String string) {
    final StringBuilder sb = new StringBuilder();

    for (int i = 0; i < string.length(); i++) {
      char character = string.charAt(i);
      if (character == '&' && string.length() > (i + 1) && validateEscapeCode(string.charAt(i + 1))) sb.append(Formatting.FORMATTING_CODE_PREFIX);
      else sb.append(character);
    }

    return sb.toString();
  }

  public static boolean validateEscapeCode (char c) {
    for (char candidate : escapeCodes) if (c == candidate) return true;
    return false;
  }
}
