package land.chipmunk.kaboomfabric.extras.commands;

import land.chipmunk.kaboomfabric.extras.Extras;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;

public interface CommandBroadcastVanilla {
  static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("broadcastvanilla")
        .requires(CommandBroadcastVanilla::requirement)
        .then(
          argument("message", greedyString())
            .executes(CommandBroadcastVanilla::tellrawCommand)
        )
    );

    dispatcher.register(literal("bcv").requires(CommandBroadcastVanilla::requirement).executes(CommandTellraw::tellrawCommand).redirect(node));
  }

  static int tellrawCommand (CommandContext<ServerCommandSource> context) {
    String string = getString(context, "message");
    Text text = Text.literal(Extras.parseEscapeSequences(string));

    context.getSource().sendFeedback(text, true);

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
