package land.chipmunk.kaboomfabric.extras.commands;

import land.chipmunk.kaboomfabric.extras.Extras;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;
import net.minecraft.network.packet.s2c.play.PositionFlag;
import net.minecraft.text.Text;
import java.util.Set;
import java.util.HashSet;

public interface CommandSpawn {
  static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("spawn")
        .requires(source -> source.hasPermissionLevel(2))
        .executes(CommandSpawn::spawnCommand)
    );
  }

  static int spawnCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final Entity entity = source.getEntityOrThrow();
    final ServerWorld world = source.getServer().getOverworld();

    final Vec3d spawn = Vec3d.ofCenter(world.getSpawnPos());
    final float spawnAngle = world.getSpawnAngle();

    final Set<PositionFlag> positionFlags = new HashSet<>();
    entity.teleport(world, spawn.getX(), spawn.getY(), spawn.getZ(), positionFlags, spawnAngle, 0);

    source.sendFeedback(Text.literal("Successfully moved to spawn"), false);

    return Command.SINGLE_SUCCESS;
  }
}
