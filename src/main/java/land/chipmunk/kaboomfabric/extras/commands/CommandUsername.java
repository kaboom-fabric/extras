package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.PropertyMap;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.PlayerManager;
import net.minecraft.network.packet.s2c.play.PlayerRemoveS2CPacket;
import net.minecraft.network.packet.s2c.play.PlayerListS2CPacket;
import land.chipmunk.kaboomfabric.extras.Extras;
import land.chipmunk.kaboomfabric.extras.mixin.PlayerEntityAccessor;

import java.util.HashMap;
import java.util.Map;
import java.util.Collections;

public abstract class CommandUsername {
  private static final DynamicCommandExceptionType ALREADY_HAVE_USERNAME_EXCEPTION = new DynamicCommandExceptionType(name -> Text.literal("You already have the username \"").append(Text.literal((String)name)).append(Text.literal("\"")));
  private static final SimpleCommandExceptionType RATELIMIT_EXCEPTION = new SimpleCommandExceptionType(Text.literal("Please wait a few seconds before changing your username."));
  private static final SimpleCommandExceptionType ALREADY_LOGGED_IN_EXCEPTION = new SimpleCommandExceptionType(Text.literal("A player with that username is already logged in."));

  public static final Map<ServerPlayerEntity, Long> lastUsedMillis = new HashMap<>();

  public static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("username")
        .requires(source -> source.hasPermissionLevel(2))
        .then(
          argument("username", greedyString())
            .executes(CommandUsername::usernameCommand)
        )
    );
  }

  public static int usernameCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();
    final PlayerManager playerManager = source.getServer().getPlayerManager();

    final GameProfile oldProfile = player.getGameProfile();
    final PropertyMap oldProperties = oldProfile.getProperties();

    final String nameColor = Extras.parseEscapeSequences(getString(context, "username"));
    final String name = nameColor.substring(0, Math.min(16, nameColor.length()));
    final long millis = lastUsedMillis.getOrDefault(player, 0L);
    final long millisDifference = System.currentTimeMillis() - millis;

    if (name.equals(oldProfile.getName())) {
      throw ALREADY_HAVE_USERNAME_EXCEPTION.create(name);
    }

    if (millisDifference <= 2000) {
      throw RATELIMIT_EXCEPTION.create();
    }

    for (ServerPlayerEntity other : source.getServer().getPlayerManager().getPlayerList()) {
      if (!other.getGameProfile().getName().equalsIgnoreCase(name)) continue;

      throw ALREADY_LOGGED_IN_EXCEPTION.create();
    }

    final GameProfile newProfile = new GameProfile(oldProfile.getId(), name);
    final PropertyMap newProperties = newProfile.getProperties();
    for (final Property property : oldProperties.values()) newProperties.put(property.getName(), property);

    ((PlayerEntityAccessor)(Object)player).gameProfile(newProfile);
    lastUsedMillis.put(player, System.currentTimeMillis());

    // Update the player's player list entry
    playerManager.sendToAll(new PlayerRemoveS2CPacket(Collections.singletonList(oldProfile.getId())));
    playerManager.sendToAll(PlayerListS2CPacket.entryFromPlayer(Collections.singleton(player)));

    source.sendFeedback(
      Text.literal("Successfully set your username to \"")
        .append(Text.literal(name))
        .append(Text.literal("\""))
    , false);
    return Command.SINGLE_SUCCESS;
  }
}
