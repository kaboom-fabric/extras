package land.chipmunk.kaboomfabric.extras.commands;

import land.chipmunk.kaboomfabric.extras.Extras;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;

public interface CommandConsole {
  static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("console")
        .requires(source -> source.hasPermissionLevel(2))
        .then(
          argument("message", greedyString())
            .executes(CommandConsole::consoleCommand)
        )
    );
  }

  static int consoleCommand (CommandContext<ServerCommandSource> context) {
    // ? Should I optimize this to manually create a ParseResults object, or just not wrap the say command in the first place?
    final ServerCommandSource source = context.getSource();
    final MinecraftServer server = source.getServer();
    final ServerCommandSource console = server.getCommandSource();

    final String command = "say " + Extras.parseEscapeSequences(getString(context, "message"));

    final CommandManager commandManager = server.getCommandManager();
    commandManager.execute(commandManager.getDispatcher().parse(command, source), command);

    return Command.SINGLE_SUCCESS;
  }
}
