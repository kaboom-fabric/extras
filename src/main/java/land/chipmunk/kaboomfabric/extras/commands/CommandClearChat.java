package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.text.MutableText;
import net.minecraft.util.Formatting;
import net.minecraft.server.network.ServerPlayerEntity;

public interface CommandClearChat {
  static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("clearchat")
        .requires(CommandClearChat::requirement)
        .executes(CommandClearChat::clearChatCommand)
    );

    dispatcher.register(literal("cc").requires(CommandClearChat::requirement).executes(CommandClearChat::clearChatCommand).redirect(node));
  }

  static int clearChatCommand (CommandContext<ServerCommandSource> context) {
    final MutableText text = Text.literal("");
    for (int i = 0; i < 100; i++) text.append(Text.literal("\n"));
    text.append(Text.literal("The chat has been cleared").formatted(Formatting.DARK_GREEN));

    for (ServerPlayerEntity player : context.getSource().getServer().getPlayerManager().getPlayerList()) {
      player.sendMessage(text);
    }

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
