package land.chipmunk.kaboomfabric.extras.commands;

import land.chipmunk.kaboomfabric.extras.Extras;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.server.network.ServerPlayerEntity;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;

public interface CommandTellraw {
  static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("broadcastraw")
        .requires(CommandTellraw::requirement)
        .then(
          argument("message", greedyString())
            .executes(CommandTellraw::tellrawCommand)
        )
    );

    dispatcher.register(literal("bcraw").requires(CommandTellraw::requirement).executes(CommandTellraw::tellrawCommand).redirect(node));
    // No tellraw alias
  }

  static int tellrawCommand (CommandContext<ServerCommandSource> context) {
    String string = getString(context, "message");
    Text text = Text.literal(Extras.parseEscapeSequences(string));

    for (ServerPlayerEntity player : context.getSource().getServer().getPlayerManager().getPlayerList()) {
      player.sendMessage(text);
    }

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
