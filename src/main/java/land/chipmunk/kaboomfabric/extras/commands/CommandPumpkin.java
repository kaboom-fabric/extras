package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.argument.EntityArgumentType.players;
import static net.minecraft.command.argument.EntityArgumentType.getPlayers;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import java.util.Collection;

public interface CommandPumpkin {
  static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("pumpkin")
        .requires(source -> source.hasPermissionLevel(2))
        .then(
          argument("targets", players())
            .executes(CommandPumpkin::pumpkinCommand)
        )
    );
  }

  static int pumpkinCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final Collection<ServerPlayerEntity> players = getPlayers(context, "targets");
    final Item carvedPumpkin = Registries.ITEM.get(new Identifier("minecraft", "carved_pumpkin"));

    for (ServerPlayerEntity player : players) {
      final PlayerInventory inventory = player.getInventory();
      inventory.setStack(39, new ItemStack(carvedPumpkin));
    }

    if (players.size() == 1) {
      final ServerPlayerEntity player = (ServerPlayerEntity) players.toArray()[0];

      source.sendFeedback(
        Text.literal("Player \"")
          .append(Text.literal(player.getGameProfile().getName()))
          .append(Text.literal("\" is now a pumpkin"))
      , true);

      return Command.SINGLE_SUCCESS;
    }

    source.sendFeedback(Text.literal("Multiple players are now pumpkins"), true);

    return Command.SINGLE_SUCCESS;
  }
}
