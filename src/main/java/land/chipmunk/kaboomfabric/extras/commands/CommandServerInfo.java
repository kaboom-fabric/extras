package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.text.MutableText;
import net.minecraft.util.Formatting;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.*;
import java.net.InetAddress;

public abstract class CommandServerInfo {
  public static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("serverinfo")
        .requires(CommandServerInfo::requirement)
        .executes(CommandServerInfo::serverInfoCommand)
    );

    dispatcher.register(literal("specs").requires(CommandServerInfo::requirement).executes(CommandServerInfo::serverInfoCommand).redirect(node));
  }

  private static void sendInfoMessage (ServerCommandSource source, String description, String value) {
    source.sendFeedback(
      Text.literal(description).formatted(Formatting.GRAY)
        .append(Text.literal(": " + value).formatted(Formatting.WHITE))
    , false);
  }

  public static int serverInfoCommand (CommandContext<ServerCommandSource> context) {
    final ServerCommandSource source = context.getSource();
    final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
    final RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    final MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
    final Runtime runtime = Runtime.getRuntime();

    try {
      final InetAddress localhost = InetAddress.getLocalHost();
      sendInfoMessage(source, "Hostname", localhost.getHostName());
      sendInfoMessage(source, "IP address", localhost.getHostAddress());
    } catch (Exception ignored) {
    }

    sendInfoMessage(source, "OS name", operatingSystemMXBean.getName());
    sendInfoMessage(source, "OS architecture", operatingSystemMXBean.getArch());
    sendInfoMessage(source, "OS version", operatingSystemMXBean.getVersion());

    sendInfoMessage(source, "Java VM", runtimeMXBean.getVmName());
    sendInfoMessage(source, "Java version", runtimeMXBean.getSpecVersion());

    try {
      final String[] shCommand = {
        "/bin/sh",
        "-c",
        "cat /proc/cpuinfo | grep 'model name' | cut -f 2 -d ':' | awk '{$1=$1}1' | head -1"
      };

      final Process process = runtime.exec(shCommand);
      final InputStreamReader isr = new InputStreamReader(process.getInputStream());
      final BufferedReader br = new BufferedReader(isr);
      String line;

      while ((line = br.readLine()) != null) {
        sendInfoMessage(source, "CPU model", line);
      }

      br.close();
    } catch (Exception ignored) {
    }

    sendInfoMessage(source, "CPU cores", String.valueOf(runtime.availableProcessors()));
    sendInfoMessage(source, "CPU load", String.valueOf(operatingSystemMXBean.getSystemLoadAverage()));

    final long heapUsage = memoryMXBean.getHeapMemoryUsage().getUsed();
    final long nonHeapUsage = memoryMXBean.getNonHeapMemoryUsage().getUsed();
    final long memoryMax = (
      memoryMXBean.getHeapMemoryUsage().getMax() +
      memoryMXBean.getNonHeapMemoryUsage().getMax()
    );
    final long memoryUsage = (heapUsage + nonHeapUsage);

    sendInfoMessage(source, "Available memory", (memoryMax / 1024 / 1024) + "MB");
    sendInfoMessage(source, "Heap memory usage", (heapUsage / 1024 / 1024) + "MB");
    sendInfoMessage(source, "Non-heap memory usage", (nonHeapUsage / 1024 / 1024) + "MB");
    sendInfoMessage(source, "Total memory usage", (memoryUsage / 1024 / 1024) + "MB");

    final long minutes = (runtimeMXBean.getUptime() / 1000) / 60;
    final long seconds = (runtimeMXBean.getUptime() / 1000) % 60;

    sendInfoMessage(source, "Server uptime", minutes + " minute(s) " + seconds + " second(s)");

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
