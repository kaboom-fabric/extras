package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static net.minecraft.server.command.CommandManager.literal;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.registry.Registries;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;

public abstract class CommandEnchantAll {
  private static final SimpleCommandExceptionType EMPTY_ITEM_EXCEPTION = new SimpleCommandExceptionType(Text.literal("Please hold an item in your hand to enchant it"));

  public static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("enchantall")
        .requires(source -> source.hasPermissionLevel(2))
        .executes(CommandEnchantAll::enchantAllCommand)
    );
  }

  public static int enchantAllCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();
    final PlayerInventory inventory = player.getInventory();

    final NbtList enchantments = new NbtList();
    for (Identifier identifier : Registries.ENCHANTMENT.getIds()) {
      final NbtCompound enchantment = new NbtCompound();
      enchantment.putString("id", identifier.toString());
      enchantment.putShort("lvl", (short) 0xFF);
      enchantments.add(enchantment);
    }

    final ItemStack stack = inventory.getStack(inventory.selectedSlot).copy();
    if (stack.isEmpty()) throw EMPTY_ITEM_EXCEPTION.create();

    NbtCompound nbt = stack.getNbt();
    if (nbt == null) {
      nbt = new NbtCompound();
      stack.setNbt(nbt);
    }
    stack.getNbt().put("Enchantments", enchantments);
    inventory.setStack(inventory.selectedSlot, stack);

    source.sendFeedback(Text.literal("I killed Martin."), false);

    return Command.SINGLE_SUCCESS;
  }
}
