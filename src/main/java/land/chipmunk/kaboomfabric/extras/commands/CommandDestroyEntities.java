package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.MinecraftServer;
import net.minecraft.text.Text;

// Currently broken
public interface CommandDestroyEntities {
  static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("destroyentities")
        .requires(CommandDestroyEntities::requirement)
        // .executes(CommandDestroyEntities::destroyEntitiesCommand)
    );

    dispatcher.register(literal("de").requires(CommandDestroyEntities::requirement).redirect(node));
  }

  static int destroyEntitiesCommand (CommandContext<ServerCommandSource> context) {
    final ServerCommandSource source = context.getSource();
    final MinecraftServer server = source.getServer();

    int entityCount = 0;
    int worldCount = 0;
    for (ServerWorld world : server.getWorlds()) {
      for (Entity entity : world.iterateEntities()) {
        if (entity instanceof PlayerEntity) continue;
        try {
          entity.discard();
          entityCount++;
        } catch (Exception ignored) {
        }
      }
      worldCount++;
    }

    source.sendFeedback(
      Text.literal("Successfully destroyed ")
        .append(Text.literal(String.valueOf(entityCount)))
        .append(Text.literal(" entities in "))
        .append(Text.literal(String.valueOf(worldCount)))
        .append(Text.literal(" worlds")),
    true);

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
