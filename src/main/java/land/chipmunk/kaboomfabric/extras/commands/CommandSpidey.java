package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.entity.Entity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;
import land.chipmunk.kaboomfabric.extras.modules.block.BlockIterator;

public abstract class CommandSpidey {
  public static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("spidey")
        .requires(source -> source.hasPermissionLevel(2))
        // .executes(CommandSpidey::spideyCommand)
    );
  }

  public static int spideyCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final Entity entity = source.getEntityOrThrow();
    final ServerWorld world = source.getWorld();

    final Vec3d start = entity.getEyePos();
    final Vec3d direction = Vec3d.fromPolar(entity.getYaw(), entity.getPitch());
    final int yOffset = 0;
    final int distance = 50;

    final BlockIterator iterator = new BlockIterator(start, direction, yOffset, distance);

    final BlockState cobweb = Registries.BLOCK.get(new Identifier("minecraft", "cobweb")).getDefaultState();

    while (iterator.hasNext()) {
      BlockPos pos = iterator.next();
      if (!world.isInBuildLimit(pos) || !world.getBlockState(pos).isAir()) break;

      if (world.getBlockState(pos).hasBlockEntity()) world.removeBlockEntity(pos);
      world.setBlockState(pos, cobweb);
    }

    return Command.SINGLE_SUCCESS;
  }
}
