package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static net.minecraft.server.command.CommandManager.literal;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.world.World;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.registry.Registries;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;
import net.minecraft.text.Text;
import java.util.concurrent.ThreadLocalRandom;

public interface CommandKaboom {
  static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("kaboom")
        .requires(source -> source.hasPermissionLevel(2))
        .executes(CommandKaboom::kaboomCommand)
    );
  }

  static int kaboomCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerPlayerEntity player = context.getSource().getPlayerOrThrow();
    boolean explode = ThreadLocalRandom.current().nextBoolean();

    if (explode) {
      final Vec3d position = player.getPos();
      final ServerWorld world = player.getWorld();
      final int explosionCount = 20;
      final int power = 8;

      world.createExplosion(player, position.getX(), position.getY(), position.getZ(), power, true, World.ExplosionSourceType.MOB);


      final int power2 = 4;
      final BlockState lava = Registries.BLOCK.get(new Identifier("minecraft", "lava")).getDefaultState();

      for (int i = 0; i < explosionCount; i++) {
        final double posX = position.getX() + ThreadLocalRandom.current().nextInt(-15, 15);
        final double posY = position.getY() + ThreadLocalRandom.current().nextInt(-6, 6);
        final double posZ = position.getZ() + ThreadLocalRandom.current().nextInt(-15, 15);

        world.createExplosion(player, posX, posY, posZ, power2, true, World.ExplosionSourceType.MOB);

        final BlockPos blockPos = new BlockPos((int) posX, (int) posY, (int) posZ);
        if (!world.canSetBlock(blockPos)) continue;
        if (world.getBlockState(blockPos).hasBlockEntity()) world.removeBlockEntity(blockPos);
        world.setBlockState(blockPos, lava);
      }

      player.sendMessage(Text.literal("Forgive me :c"));
      return Command.SINGLE_SUCCESS;
    }

    final PlayerInventory inventory = player.getInventory();
    inventory.setStack(inventory.selectedSlot, new ItemStack(Registries.ITEM.get(new Identifier("minecraft", "cake"))));
    player.sendMessage(Text.literal("Have a nice day :)"));
    return Command.SINGLE_SUCCESS;
  }
}
